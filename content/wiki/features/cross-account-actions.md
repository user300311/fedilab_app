*[Home](../../home) > [Features](../../features) &gt;* Cross account actions

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Cross Account Actions
This feature allows to boost or to add to your favorites a toot with another account

**This feature will work with accounts that are used with the app**

<img src="../../res/cross_account_actions/cross_account_1.png" width="250px">

1. Now, whatever the account you are using, if you long press a boost icon or a favorite icon, a popup will display the list of your accounts.<br><img src="../../res/cross_account_actions/cross_account_2.png" width="300px">

2. A message will let you know that the toot was successfully boosted/favorited with the other account.

3. Of course, You can check the result by changing the account<br><img src="../../res/cross_account_actions/cross_account_3.png" width="300px">

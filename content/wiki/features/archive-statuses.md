*[Home](../../home) > [Features](../../features) &gt;* Archive statuses

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Archive Statuses
This feature archives statuses in a CSV file.

1. **Open the left menu and click on the three vertical dots**

2. **Then click on "Export statuses"** <br> *The app will work in background looking at all your statuses, so it can be quite long* <br><br> <img src="../../res/archive/export_1.png" width="250px"> <br> <img src="../../res/archive/export_2.png" width="250px">

3. **When all data are exported, you are notified**<br>*A CSV file is created where boosts are discarded, that's why the number of exported statuses is lower than the total of your toots.* <br><br> <img src="../../res/archive/export_3.png" width="300px">
